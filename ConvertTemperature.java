package org.example;

import java.util.Scanner;

public class ConvertTemperature {
    public static void main( String[] args )
    {
        float f, c, n;
        Scanner scan = new Scanner(System.in);
        System.out.println("Nhập nhiệt độ F: ");
        f = scan.nextFloat();
        c = (f-32)/9*5;
        System.out.println("Nhiệt độ " +f+ "F convert sang C là: " +c );

        System.out.println("Nhập nhiệt độ C: ");
        c = scan.nextFloat();
        f = (c/5*9+32);
        System.out.println("Nhiệt độ " +c+ "C convert sang f là: " +f );

    }
}
